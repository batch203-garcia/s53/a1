/* import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown'; */

import {Container, Nav, Navbar} from "react-bootstrap"
import {NavLink} from "react-router-dom"
import { useState } from "react"

export default function AppNavbar(){

const [user, setUser] = useState(localStorage.getItem("email"));
console.log(user)


    return(
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand as={NavLink} to="/">Zuitt</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ms-auto">
                    <Nav.Link as={NavLink} to="/" end>Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                    {
                        (user !== null)
                        ?
                        <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                        :
                        <>
                            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                    <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                        </>

                    }
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

