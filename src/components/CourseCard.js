import { useState, useEffect } from "react";
import {Button, Card} from "react-bootstrap"

export default function CoursesCard({courseProp}){
    const {id, name, description, price} = courseProp;

    const [count, setCount] = useState(0);
    const [seats, setSeat] = useState(30);
    const [disabled, setDisabled] = useState(false);

   /*  useEffect(() =>{
        console.log("Initial render, for every changes");
    }) */

    /* useEffect(()=> {
        console.log("will only run initially")
    }, []) */

    /* useEffect(()=> {
        console.log("will only run initially and every changes on dependency")
    }, [seats]) */

    /*  function unEnroll(){
        setCount(count - 1);
    } */

    function enroll(){

        //ACTIVITY SOLUTION
        /*  let btnEnroll = document.querySelector(`#{id}`);
        if(seats === 0){
            alert("No more available seats.");
            btnEnroll.setAttribute("disabled", true);
            btnEnroll.innerText = "FULL";
            btnEnroll.classList.add('btn-danger');
            console.log(btnEnroll);
        }else{
            setCount(count + 1);
            setSeat(seats - 1);
        } */

        setCount(count + 1);
        setSeat(seats - 1);
    }

    useEffect(()=>{
        if(seats <= 0){
            setDisabled(true);
            alert("No more seats available");
        }
    }, [seats])

    return(
                <Card className="my-3">
                    <Card.Body>
                        <Card.Title>
                            {name}
                        </Card.Title>
                        <Card.Subtitle>
                            Description:
                        </Card.Subtitle>
                        <Card.Text>
                        {description}
                        </Card.Text>
                        <Card.Subtitle>
                            Price:
                        </Card.Subtitle>
                        <Card.Text>
                        Php {price}
                        </Card.Text>
                        <Card.Subtitle>
                            Enrollees:
                        </Card.Subtitle>
                        <Card.Text className="text-success">
                        {count} - Enrollees
                        </Card.Text>
                        <Button disabled={disabled} id={id} variant="primary"  onClick={enroll}>Enroll</Button>
                    </Card.Body>
                </Card>
    )
}