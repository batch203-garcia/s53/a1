import {Col, Row, Button} from "react-bootstrap"
import {Link} from 'react-router-dom'

export default function Banner({bannerProp}){

    return(
        
        <Row>
            <Col className="p-5 text-center">
                <h1>{bannerProp.title}</h1>
                <p>{bannerProp.description}</p>
                <Button as={Link} to={bannerProp.destination} variant="primary">{bannerProp.label}</Button>
            </Col>
        </Row>

    )
}