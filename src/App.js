import { Container } from 'react-bootstrap';
import './App.css';
import AppNavbar from './components/AppNavbar';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import Courses from './pages/Courses';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Error from './pages/Error';

function App() {
  return (
    <div className="App">
      <Router>
      <AppNavbar/>

      <Container>
        <Routes>
          <Route exact path='/' element={<Home/>}/>
          <Route exact path='/courses' element={<Courses/>}/>
          <Route exact path='/login' element={<Login/>}/>
          <Route exact path='/register' element={<Register/>}/>
          <Route exact path='/logout' element={<Logout/>}/>
          <Route exact path='*' element={<Error/>}/>
        </Routes>
      </Container>
      </Router>
    </div>
  );
}

{/* <Home/>
          <Courses/>
          <Register/>
          <Login/> */}

export default App;
