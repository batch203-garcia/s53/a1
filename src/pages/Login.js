import {Button, Form} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';

export default function Login(){
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');

const [isActive, setIsActive] = useState(false);
console.log(email)
console.log(password)

const navigate = useNavigate();


useEffect(()=>{

    if((email !== '' &&  password !== '') && (email === "admin@mail.com" && password === "admin123")){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

}, [email, password]);

function authenticate(e){
    e.preventDefault();

        localStorage.setItem("email", email);

    setEmail('');
    setPassword('');


    alert(`${email}, You are now logged in!`);
    navigate("/");
}

    return(
        <>
        <h1 className='my-5 text-center'>Login</h1>
            <Form onSubmit={e => authenticate(e)}>
                <Form.Group className="mb-3" controlId="emailAddress">
                <Form.Label>Email Address</Form.Label>
                <Form.Control type="emailAddress" placeholder="Enter Email" required value={email} onChange={e => setEmail(e.target.value)}/>
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Enter Password" required value={password}  onChange={e => setPassword(e.target.value)}/>
                </Form.Group>

                {
                    isActive
                    ?
                    <Button className='w-25' variant="primary" type="loginBtn" >
                    Login
                    </Button>
                    :
                    <Button className='w-25' variant="danger" type="loginBtn" disabled>
                    Login
                    </Button>
                }
            </Form>
        </>
    )
}