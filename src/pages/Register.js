import {Button, Form} from 'react-bootstrap';
import {useState, useEffect} from 'react';

export default function Register(){
const [fName, setFName] = useState('');
const [lName, setLName] = useState('');
const [mobileNo, setMobileNo] = useState('');
const [email, setEmail] = useState('');
const [password1, setPassword1] = useState('');
const [password2, setPassword2] = useState('');

const [isActive, setIsActive] = useState(false);
console.log(fName)
console.log(lName)
console.log(email)
console.log(mobileNo)
console.log(password1)
console.log(password2)

useEffect(()=>{

    if((fName !== '' && lName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

}, [fName, lName, email, mobileNo, password1, password2]);

function registerUser(e){
    e.preventDefault();

    setFName('');
    setLName('');
    setEmail('');
    setMobileNo('');
    setPassword1('');
    setPassword2('');

    alert("Thank you for registering!");
}

    return(
        <>
        <h1 className='my-5 text-center'>Register</h1>
            <Form onSubmit={e => registerUser(e)}>
                <Form.Group className="mb-3" controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control type="text"  placeholder="Enter First Name" required value={fName} onChange={e => setFName(e.target.value)}/>
                </Form.Group>

                <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control type="text" placeholder="Enter Last Name" required value={lName} onChange={e => setLName(e.target.value)}/>
                </Form.Group>

                <Form.Group className="mb-3" controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control type="number" placeholder="09xxxxxxxxx" required value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
                </Form.Group>

                <Form.Group className="mb-3" controlId="emailAddress">
                <Form.Label>Email Address</Form.Label>
                <Form.Control type="emailAddress" placeholder="Enter Email" required value={email} onChange={e => setEmail(e.target.value)}/>
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Enter Password" required value={password1}  onChange={e => setPassword1(e.target.value)}/>
                </Form.Group>

                <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control type="password" placeholder="Verify Password" required value={password2}  onChange={e => setPassword2(e.target.value)}/>
                </Form.Group>

                {
                    isActive
                    ?
                    <Button className='w-25' variant="primary" type="submitBtn" >
                    Submit
                    </Button>
                    :
                    <Button className='w-25' variant="danger" type="submitBtn" disabled>
                    Submit
                    </Button>
                }
            </Form>
        </>
    )
}